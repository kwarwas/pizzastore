﻿interface IPizza {
    _id: number;
    name: string;
    description: string;
    price: number;
    imageUrl: string;
} 