﻿/// <reference path="../../PizzaServer/models/IPizza.ts" />

module Pizza {
    'use strict';
    export class PizzaService {

        public static $inject = [
            '$http'
        ];

        constructor(private $http: ng.IHttpService) {
        }

        public getPizza(): ng.IPromise<IPizza[]> {
            return this.$http.get('//localhost:1339/pizzaApi');
        }
    }

    angular
        .module('PizzaApp')
        .service('PizzaService', PizzaService);
}

