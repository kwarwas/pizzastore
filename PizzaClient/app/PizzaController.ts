﻿/// <reference path="../../PizzaServer/models/IPizza.ts" />
/// <reference path="app.ts" />

module Pizza {
    'use strict';

    class PizzaControlller {

        public data: IPizza[];

        public static $inject = [
            'PizzaService'
        ];

        constructor(private PizzaService: PizzaService) {
            PizzaService.getPizza().then((x: ng.IHttpPromiseCallbackArg<IPizza[]>): void => {
                this.data = x.data;
            });
        }
    }

    angular
        .module('PizzaApp')
        .controller('PizzaController', PizzaControlller);
}