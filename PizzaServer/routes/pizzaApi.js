/// <reference path="../typings/tsd.d.ts" />
/// <reference path="../models/IPizza.ts" />
var express = require("express");
var mongodb = require("mongodb");
var router = express.Router();
var url = 'mongodb://localhost:27017/local';
router.get('/', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    mongodb.MongoClient.connect(url, function (err, db) {
        db.collection('pizza')
            .find({}).toArray(function (err, dosc) {
            dosc.forEach(function (item) { return item.price *= 2; });
            res.json(dosc).end();
            db.close();
        });
    });
});
module.exports = router;
//mongodb.MongoClient.connect(url, (err, db) => {
//    var collection: mongodb.Collection = db.collection('pizza');
//    collection.find({}).toArray((err, docs: IPizza[]) => {
//        docs.forEach((item: IPizza) => item.price *= 2);
//        res.json(docs);
//        db.close();
//    });
//});
//});
//module.exports = router; 
//# sourceMappingURL=pizzaApi.js.map