/// <reference path="../../PizzaServer/models/IPizza.ts" />
/// <reference path="app.ts" />
var Pizza;
(function (Pizza) {
    'use strict';
    var PizzaControlller = (function () {
        function PizzaControlller(PizzaService) {
            var _this = this;
            this.PizzaService = PizzaService;
            PizzaService.getPizza().then(function (x) {
                _this.data = x.data;
            });
        }
        PizzaControlller.$inject = [
            'PizzaService'
        ];
        return PizzaControlller;
    })();
    angular
        .module('PizzaApp')
        .controller('PizzaController', PizzaControlller);
})(Pizza || (Pizza = {}));
//# sourceMappingURL=PizzaController.js.map