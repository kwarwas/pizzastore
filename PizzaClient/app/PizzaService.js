/// <reference path="../../PizzaServer/models/IPizza.ts" />
var Pizza;
(function (Pizza) {
    'use strict';
    var PizzaService = (function () {
        function PizzaService($http) {
            this.$http = $http;
        }
        PizzaService.prototype.getPizza = function () {
            return this.$http.get('//localhost:1339/pizzaApi');
        };
        PizzaService.$inject = [
            '$http'
        ];
        return PizzaService;
    })();
    Pizza.PizzaService = PizzaService;
    angular
        .module('PizzaApp')
        .service('PizzaService', PizzaService);
})(Pizza || (Pizza = {}));
//# sourceMappingURL=PizzaService.js.map